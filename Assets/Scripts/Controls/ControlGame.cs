﻿using Gaminho;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlGame : MonoBehaviour {

    public EnemyControl enemyControl;
    public CallScene callScene;
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife;
    
    // Use this for initialization
    void Start () {
        PlayerPrefs.SetInt("AtualLevel", Statics.CurrentLevel);
        Statics.EnemiesDead = 0;
        Background.sprite = Levels[Statics.CurrentLevel].Background;
        TextStart.text = "Stage " + (Statics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[Statics.CurrentLevel].AudioLvl);
        
    }

    private void Update()
    {
        TextPoints.text = Statics.Points.ToString();
        BarLife.localScale = new Vector3(Statics.Life / 10f, 1, 1);
    }

    public void LevelPassed()
    {
        
        Clear();
        Statics.CurrentLevel++;
        Statics.Points += 1000 * Statics.CurrentLevel;
        if (Statics.CurrentLevel < 3)
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statics.PREFAB_CONGRATULATION) as GameObject);
        }
    }
    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        BarLife.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_GAMEOVER) as GameObject);
    }

    private void Clear()
    {
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }

    #region Pause
    public GameObject pausedText;
    private List<AudioSource> savedSounds = new List<AudioSource>();

    public void PauseGame()
    {
        Time.timeScale = Time.timeScale == 1 ? 0 : 1;
        callScene.isPaused = Time.timeScale == 1 ? false : true;
        pausedText.SetActive(Time.timeScale == 1 ? false : true);
        if (Time.timeScale == 0)
        {
            PauseAudioSources();
        }
        else
        {
            PlayAudioSources();
        }
    }

    public void PauseAudioSources()
    {
        AudioSource[] sounds = GameObject.FindObjectsOfType<AudioSource>();
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].isPlaying)
            {
                savedSounds.Add(sounds[i]);
                sounds[i].Pause();
            }
        }
        GetComponent<AudioSource>().Stop();
    }

    public void PlayAudioSources()
    {
        for (int i = 0; i < savedSounds.Count; i++)
        {
            savedSounds[i].Play();
        }
        savedSounds.Clear();
        GetComponent<AudioSource>().clip = enemyControl.bossCalled ? enemyControl.AudioBoss : Levels[Statics.CurrentLevel].AudioLvl;
        GetComponent<AudioSource>().Play();
    }
    #endregion
}
