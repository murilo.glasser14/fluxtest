﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlStart : MonoBehaviour
{
    public Text Record;
    // Use this for initialization
    void Start()
    {
        //Reset the variables to start the game from scratch
        Statics.WithShield = false;
        Statics.EnemiesDead = 0;
        Statics.Points = 0;
        Statics.ShootingSelected = 2;
        //Loads Record
        if (PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) == 0)
        {
            PlayerPrefs.SetString(Statics.PLAYERPREF_NEWRECORD, "Nobody");
        }
        Record.text = "Record: " + PlayerPrefs.GetString(Statics.PLAYERPREF_NEWRECORD) + "(" + PlayerPrefs.GetInt(Statics.PLAYERPREF_VALUE) + ")";

    }

    public void StartClick()
    {
/*#if !UNITY_EDITOR
        Debug.Log("Σφάλμα σκόπιμα, το βρήκατε, συγχαρητήρια!");
        Sair();
        return;
#endif*/
        Statics.CurrentLevel = 0;
        PlayerPrefs.SetInt("AtualLevel", Statics.CurrentLevel);
        GetComponent<AudioSource>().Stop();
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_HISTORY) as GameObject);
    }

    public void Continue()
    {
        if (PlayerPrefs.HasKey("AtualLevel"))
            Statics.CurrentLevel = PlayerPrefs.GetInt("AtualLevel");
        else
            Statics.CurrentLevel = 0;

        GetComponent<AudioSource>().Stop();
        GameObject.Instantiate(Resources.Load(Statics.PREFAB_HISTORY) as GameObject);
    }

    public void Quit()
    {
        PlayerPrefs.SetInt("AtualLevel", Statics.CurrentLevel);
        Application.Quit();
    }


}
