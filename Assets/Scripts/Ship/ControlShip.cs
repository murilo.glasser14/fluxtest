﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using Gaminho;

public class ControlShip : MonoBehaviour
{
    #region Public
    public bool isGhost = false;
    public float Velocity = 10f;
    public float SpeedRotation = 200.0f;
    public ControlGame controlGame;
    public Life life;
    public GameObject MotorAnimation;
    public GameObject Shield;
    public GameObject Explosion;
    public Shot[] Shots;
    public Slider shieldSlider;
    #endregion

    #region Private
    private Vector3 startPos;
    private bool shooting = false;
    private int lifeShield;
    private GameObject follow;
    private Vector2 lastPosition;
    private Quaternion lastRotation;

    #endregion

    void Start()
    {
        follow = isGhost ? GameObject.Find("Ship") : null;

        if (!isGhost)
        {
            Statics.Player = gameObject.transform;

            if (GetComponent<Rigidbody2D>() == null)
            {
                Debug.LogError("Component required Rigidbody2D");
                Destroy(this);
                return;
            }

            if (GetComponent<BoxCollider2D>() == null)
            {
                Debug.LogWarning("BoxCollider2D not found, adding ...");
                gameObject.AddComponent<BoxCollider2D>();

            }

            startPos = transform.localPosition;
            GetComponent<Rigidbody2D>().gravityScale = 0.001f;

            StartCoroutine(Shoot());
        }
        else
        {
            if (GetComponent<Rigidbody2D>() == null)
            {
                Debug.LogError("Component required Rigidbody2D");
                Destroy(this);
                return;
            }

            if (GetComponent<BoxCollider2D>() == null)
            {
                Debug.LogWarning("BoxCollider2D not found, adding ...");
                gameObject.AddComponent<BoxCollider2D>();

            }

            startPos = transform.localPosition;
            GetComponent<Rigidbody2D>().gravityScale = 0.001f;

        }
    }


    void Update()
    {
        #region Move
        if (!isGhost)
        {
            float rotation = Input.GetAxis("Horizontal") * SpeedRotation;
            rotation *= Time.deltaTime;
            transform.Rotate(0, 0, -rotation);


            if (Input.GetAxis("Vertical") != 0)
            {
                Vector2 translation = Input.GetAxis("Vertical") * new Vector2(0, Velocity * GetComponent<Rigidbody2D>().mass);
                translation *= Time.deltaTime;
                GetComponent<Rigidbody2D>().AddRelativeForce(translation, ForceMode2D.Impulse);
            }
            AnimateMotor();

            if (transform.localPosition.y > controlGame.ScenarioLimit.yMax || transform.localPosition.y < controlGame.ScenarioLimit.yMin || transform.localPosition.x > controlGame.ScenarioLimit.xMax || transform.localPosition.x < controlGame.ScenarioLimit.xMin)
            {
                Vector3 dir = startPos - transform.localPosition;
                dir = dir.normalized;
                GetComponent<Rigidbody2D>().AddForce(dir * (2 * GetComponent<Rigidbody2D>().mass), ForceMode2D.Impulse);

            }


            #region Tiro
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                shooting = true;
            }
            if (Input.GetKeyUp(KeyCode.LeftControl))
            {
                shooting = false;
            }
            #endregion
            Statics.Life = life.life;

        }
        else
        {
            lastPosition = follow.transform.position;
            lastRotation = follow.transform.rotation;

        }

        #endregion


    }


    void LateUpdate()
    {
        #region Move
        if(follow)
        {
            transform.position = Vector2.Lerp(transform.position, lastPosition, 4f * Time.deltaTime);
            transform.rotation = Quaternion.Lerp(transform.rotation, lastRotation, 1.5f * Time.deltaTime);
        }
        #endregion
    }


    private void AnimateMotor()
    {
        if (MotorAnimation.activeSelf != (Input.GetAxis("Vertical") != 0))
        {
            MotorAnimation.SetActive(Input.GetAxis("Vertical") != 0);
        }
    }


    public static void AddBulletToDogeEnemys(Transform bullet)
    {
        Enemy[] enemys = GameObject.FindObjectsOfType<Enemy>();
        for (int i = 0; i < enemys.Length; i++)
        {
            if (enemys[i].dodge)
            {
                if (enemys[i].playerBullets.Contains(bullet))
                    return;

                enemys[i].playerBullets.Add(bullet);
            }
        }
    }

    public static void RemoveBulletToDodgeEnemys(Transform bullet)
    {
        Enemy[] enemys = GameObject.FindObjectsOfType<Enemy>();
        for (int i = 0; i < enemys.Length; i++)
        {
            if (enemys[i].dodge)
                enemys[i].playerBullets.Remove(bullet);
        }
    }

    private IEnumerator Shoot()
    {
        while (true)
        {
            yield return new WaitForSeconds(Shots[Statics.ShootingSelected].ShootingPeriod);
            if (shooting)
            {
                Statics.Damage = Shots[Statics.ShootingSelected].Damage;
                GameObject goShoot = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                goShoot.transform.parent = transform;
                goShoot.transform.localPosition = Shots[Statics.ShootingSelected].Weapon.transform.localPosition;
                goShoot.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                goShoot.AddComponent<BoxCollider2D>();
                goShoot.transform.parent = transform.parent;
                AddBulletToDogeEnemys(goShoot.transform);

                if (Shots[Statics.ShootingSelected].TypeShooter == Statics.TYPE_SHOT.DOUBLE)
                {
                    GameObject goShoot2 = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                    goShoot2.transform.parent = transform;
                    goShoot2.transform.localPosition = Shots[Statics.ShootingSelected].Weapon2.transform.localPosition;
                    goShoot2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShoot2.AddComponent<BoxCollider2D>();
                    goShoot2.transform.parent = transform.parent;
                    AddBulletToDogeEnemys(goShoot2.transform);

                }

                if (Shots[Statics.ShootingSelected].TypeShooter == Statics.TYPE_SHOT.TRIPLE)
                {
                    GameObject goShoot2 = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                    goShoot2.transform.parent = transform;
                    goShoot2.transform.localPosition = Shots[Statics.ShootingSelected].Weapon2.transform.localPosition;
                    goShoot2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShoot2.AddComponent<BoxCollider2D>();
                    goShoot2.transform.parent = transform.parent;

                    GameObject goTiro3 = Instantiate(Shots[Statics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                    goTiro3.transform.parent = transform;
                    goTiro3.transform.localPosition = Shots[Statics.ShootingSelected].Weapon3.transform.localPosition;
                    goTiro3.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goTiro3.AddComponent<BoxCollider2D>();
                    goTiro3.transform.parent = transform.parent;
                    AddBulletToDogeEnemys(goShoot2.transform);
                    AddBulletToDogeEnemys(goTiro3.transform);

                }
            }

            CallShield();
        }
    }

    private void CallShield()
    {
        if (Shield.activeSelf != Statics.WithShield)
        {
            shieldSlider.value = 10;
            shieldSlider.gameObject.SetActive(Statics.WithShield);
            Shield.SetActive(Statics.WithShield);
        }
    }

    private void OnCollisionEnter2D(Collision2D obj)
    {

        if (obj.gameObject.tag == "Enemy")
        {
            Instantiate(Explosion, transform);

            if (Statics.WithShield)
            {
                lifeShield--;
                shieldSlider.value = lifeShield;
            }
            else
            {

                life.TakesLife(obj.gameObject.GetComponent<Enemy>().Damage);
            }

        }

        if (obj.gameObject.tag == "Shot" || obj.gameObject.tag == "EnemyShot")
        {
            if (obj.gameObject.tag == "Shot")
                RemoveBulletToDodgeEnemys(obj.gameObject.transform);

            Instantiate(Explosion, transform);

            if (Statics.WithShield)
            {
                lifeShield--;
                shieldSlider.value = lifeShield;
            }
            else
            {

                life.TakesLife(1);
            }
            Destroy(obj.gameObject);
        }

        if (lifeShield <= 0)
        {
            shieldSlider.gameObject.SetActive(false);
            Statics.WithShield = false;
            lifeShield = 10;
        }


    }

}


