﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct  Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct  CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct  CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct  DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct  PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct  RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct  StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct  HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct  IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct  DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct  DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void ControlGame_t311D836B3FE34B0182B188639EC992A3B193DC96_CustomAttributesCacheGenerator_TextStart(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
}
static void EnemyControl_t8354619B7C2514F856067E693DC1914D07EE53D9_CustomAttributesCacheGenerator_EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_0_0_0_var), NULL);
	}
}
static void EnemyControl_t8354619B7C2514F856067E693DC1914D07EE53D9_CustomAttributesCacheGenerator_EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_0_0_0_var), NULL);
	}
}
static void U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator_U3CProcessU3Ed__5__ctor_m9EB907EB96FE898721FBC65CE3C54E7CFAA5C9B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator_U3CProcessU3Ed__5_System_IDisposable_Dispose_m00B8B03C83A0EABF699CBF08F14626CFB04183BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator_U3CProcessU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E271193F0B2FD02B2FEE648C61EF68D6D4D0501(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator_U3CProcessU3Ed__5_System_Collections_IEnumerator_Reset_mD794B85236A6D135A35F1E2D251AEC1350FF1BB7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator_U3CProcessU3Ed__5_System_Collections_IEnumerator_get_Current_m3E0C3125756F18616C112D57F2F5240EAECAB7FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator_U3CCallBossU3Ed__7__ctor_m6306AC9036C8D72FD585EE9EF3D3BDE61257C853(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator_U3CCallBossU3Ed__7_System_IDisposable_Dispose_m7DC3BD63F213D4A573FAAC214A03C43D9BBB3422(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator_U3CCallBossU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AEB18A69A9D6490183794EE916A477716BA54AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator_U3CCallBossU3Ed__7_System_Collections_IEnumerator_Reset_m7F8D5D398C1810F019D5BFD7FF9FC95AC91A7A4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator_U3CCallBossU3Ed__7_System_Collections_IEnumerator_get_Current_m6EA8178F2E1F71F2D655440F4A8FB0A79F9F5BAA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Boss2_t1ED338A653D36E5352D40483F92F9C50B359FEBC_CustomAttributesCacheGenerator_Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_0_0_0_var), NULL);
	}
}
static void Boss2_t1ED338A653D36E5352D40483F92F9C50B359FEBC_CustomAttributesCacheGenerator_Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_0_0_0_var), NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_0_0_0_var), NULL);
	}
}
static void Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_0_0_0_var), NULL);
	}
}
static void Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_0_0_0_var), NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_DodgeToNewPosition_m6F09C0B0C29172A7AE00EA4BA82B70C409FB5FEB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_0_0_0_var), NULL);
	}
}
static void Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_0_0_0_var), NULL);
	}
}
static void Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_0_0_0_var), NULL);
	}
}
static void U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator_U3CDodgeToNewPositionU3Ed__15__ctor_m0E5A97B8E139B274C51E2ED3DA28C562162E085B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator_U3CDodgeToNewPositionU3Ed__15_System_IDisposable_Dispose_m2BD9D44640AD35F0443D6382F8BCFA02C81B8548(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator_U3CDodgeToNewPositionU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47EFDDA5A8A24A0A3E180713F136D9DF1DBDDFFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator_U3CDodgeToNewPositionU3Ed__15_System_Collections_IEnumerator_Reset_m87CA9FF4689F13B383F7A25D6B833BAA192862F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator_U3CDodgeToNewPositionU3Ed__15_System_Collections_IEnumerator_get_Current_mDE156AF9CFC0DA387BE2142E40AE320BC8C5F463(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator_U3CKillMeU3Ed__18__ctor_mB6D7E5ADBA40D740E62FAEEB83A7F8A95AECBF62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator_U3CKillMeU3Ed__18_System_IDisposable_Dispose_m7EE76697DA279F061C9E2CB673439B3B9AD61B05(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator_U3CKillMeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m608652410A36CEA3719C34427C8FDBCBD80351D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator_U3CKillMeU3Ed__18_System_Collections_IEnumerator_Reset_mBFA75F8939D22AA066732B49BF854724496D7CC7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator_U3CKillMeU3Ed__18_System_Collections_IEnumerator_get_Current_m1B6B8DE350B6F946FA80D6E56B06F197B2C38621(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator_U3CShootU3Ed__20__ctor_mA4F447BAD55E437F71D077495DA28DC1511039C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator_U3CShootU3Ed__20_System_IDisposable_Dispose_mA84149F7BF21951275C5216F3E6F69331CA5F101(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator_U3CShootU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6299308E86DC71DFB95538A4E96554BC36FDE9ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator_U3CShootU3Ed__20_System_Collections_IEnumerator_Reset_m830371A449507FC6B227D7EEAAD7643E4776D655(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator_U3CShootU3Ed__20_System_Collections_IEnumerator_get_Current_mCBD03C6B95EC8B23F96C796EDDA096B2FDF30F9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DestroyTime_t24806801B4F6EA764796D3C1789E9D157D0023D2_CustomAttributesCacheGenerator_DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_0_0_0_var), NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Explosion_t03ACA9CD2F46E4913DC45A3EA50B165F4662DCED_CustomAttributesCacheGenerator_Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_0_0_0_var), NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ControlShip_tDE8B063AEEACBC4110A6A51AF8533BED76E0D3C8_CustomAttributesCacheGenerator_ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_0_0_0_var), NULL);
	}
}
static void U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator_U3CShootU3Ed__22__ctor_m0BF6C55FBDCC191AF04C596B1E91FF554E136AF0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator_U3CShootU3Ed__22_System_IDisposable_Dispose_m29F681C63A24CE8EF48A0EE0B680713DCD4CC835(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator_U3CShootU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75B501F2E6338D92511F4038F1A414A1EFB1577A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator_U3CShootU3Ed__22_System_Collections_IEnumerator_Reset_m8D390AA67F846A37CC2FCBF11146A6AC77B07BCD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator_U3CShootU3Ed__22_System_Collections_IEnumerator_get_Current_m6C3A51F868B3F9FBD857AF22BA6B73D84431D652(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[93] = 
{
	U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator,
	U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator,
	U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator,
	U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator,
	U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator,
	U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator,
	ControlGame_t311D836B3FE34B0182B188639EC992A3B193DC96_CustomAttributesCacheGenerator_TextStart,
	EnemyControl_t8354619B7C2514F856067E693DC1914D07EE53D9_CustomAttributesCacheGenerator_EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B,
	EnemyControl_t8354619B7C2514F856067E693DC1914D07EE53D9_CustomAttributesCacheGenerator_EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101,
	U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator_U3CProcessU3Ed__5__ctor_m9EB907EB96FE898721FBC65CE3C54E7CFAA5C9B5,
	U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator_U3CProcessU3Ed__5_System_IDisposable_Dispose_m00B8B03C83A0EABF699CBF08F14626CFB04183BB,
	U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator_U3CProcessU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E271193F0B2FD02B2FEE648C61EF68D6D4D0501,
	U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator_U3CProcessU3Ed__5_System_Collections_IEnumerator_Reset_mD794B85236A6D135A35F1E2D251AEC1350FF1BB7,
	U3CProcessU3Ed__5_t7277DC10F7DD797092578B4E5F5BB9A040162539_CustomAttributesCacheGenerator_U3CProcessU3Ed__5_System_Collections_IEnumerator_get_Current_m3E0C3125756F18616C112D57F2F5240EAECAB7FD,
	U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator_U3CCallBossU3Ed__7__ctor_m6306AC9036C8D72FD585EE9EF3D3BDE61257C853,
	U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator_U3CCallBossU3Ed__7_System_IDisposable_Dispose_m7DC3BD63F213D4A573FAAC214A03C43D9BBB3422,
	U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator_U3CCallBossU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AEB18A69A9D6490183794EE916A477716BA54AD,
	U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator_U3CCallBossU3Ed__7_System_Collections_IEnumerator_Reset_m7F8D5D398C1810F019D5BFD7FF9FC95AC91A7A4A,
	U3CCallBossU3Ed__7_t09838DE290FA5AAF22F541C5753FF0F42554BA1D_CustomAttributesCacheGenerator_U3CCallBossU3Ed__7_System_Collections_IEnumerator_get_Current_m6EA8178F2E1F71F2D655440F4A8FB0A79F9F5BAA,
	Boss2_t1ED338A653D36E5352D40483F92F9C50B359FEBC_CustomAttributesCacheGenerator_Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315,
	Boss2_t1ED338A653D36E5352D40483F92F9C50B359FEBC_CustomAttributesCacheGenerator_Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D,
	U3CShowPartsU3Ed__5_t09BBC03A0970E2BF56904F75C3B7E0D8B40FC34F_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62,
	U3CShotU3Ed__7_t5B6ECB25F35EA9A394F1ADE1A8C3CB4EE57063C1_CustomAttributesCacheGenerator_U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25,
	Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6,
	Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420,
	Boss3_t6C3889EF0A9F1C98DE071E5A8AE5B237CA014D60_CustomAttributesCacheGenerator_Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524,
	U3CShowPartsU3Ed__5_tC9A208D13D4CF48A5C07215906AFF39AC00370C2_CustomAttributesCacheGenerator_U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A,
	U3CAttackNowU3Ed__6_t5548957493F4260E6AFD7631F29FF348A7535DB3_CustomAttributesCacheGenerator_U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7,
	U3CAttackU3Ed__7_t08C44F054B9845EA94A3146BE9349CAAAA2E7C0C_CustomAttributesCacheGenerator_U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F,
	Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_DodgeToNewPosition_m6F09C0B0C29172A7AE00EA4BA82B70C409FB5FEB,
	Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5,
	Enemy_tF0E5C8811BC93A523814C562C545DB3C1A755627_CustomAttributesCacheGenerator_Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767,
	U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator_U3CDodgeToNewPositionU3Ed__15__ctor_m0E5A97B8E139B274C51E2ED3DA28C562162E085B,
	U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator_U3CDodgeToNewPositionU3Ed__15_System_IDisposable_Dispose_m2BD9D44640AD35F0443D6382F8BCFA02C81B8548,
	U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator_U3CDodgeToNewPositionU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47EFDDA5A8A24A0A3E180713F136D9DF1DBDDFFA,
	U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator_U3CDodgeToNewPositionU3Ed__15_System_Collections_IEnumerator_Reset_m87CA9FF4689F13B383F7A25D6B833BAA192862F0,
	U3CDodgeToNewPositionU3Ed__15_t8630BBFC8035287B48A50A3EA523DB2DF97EF4E2_CustomAttributesCacheGenerator_U3CDodgeToNewPositionU3Ed__15_System_Collections_IEnumerator_get_Current_mDE156AF9CFC0DA387BE2142E40AE320BC8C5F463,
	U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator_U3CKillMeU3Ed__18__ctor_mB6D7E5ADBA40D740E62FAEEB83A7F8A95AECBF62,
	U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator_U3CKillMeU3Ed__18_System_IDisposable_Dispose_m7EE76697DA279F061C9E2CB673439B3B9AD61B05,
	U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator_U3CKillMeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m608652410A36CEA3719C34427C8FDBCBD80351D1,
	U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator_U3CKillMeU3Ed__18_System_Collections_IEnumerator_Reset_mBFA75F8939D22AA066732B49BF854724496D7CC7,
	U3CKillMeU3Ed__18_t909B3E240E289B5829BCB58D13D1346D5E05F5FB_CustomAttributesCacheGenerator_U3CKillMeU3Ed__18_System_Collections_IEnumerator_get_Current_m1B6B8DE350B6F946FA80D6E56B06F197B2C38621,
	U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator_U3CShootU3Ed__20__ctor_mA4F447BAD55E437F71D077495DA28DC1511039C6,
	U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator_U3CShootU3Ed__20_System_IDisposable_Dispose_mA84149F7BF21951275C5216F3E6F69331CA5F101,
	U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator_U3CShootU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6299308E86DC71DFB95538A4E96554BC36FDE9ED,
	U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator_U3CShootU3Ed__20_System_Collections_IEnumerator_Reset_m830371A449507FC6B227D7EEAAD7643E4776D655,
	U3CShootU3Ed__20_tE2CB1C3B2DF1AD0DB18706FA90C953A779B1A237_CustomAttributesCacheGenerator_U3CShootU3Ed__20_System_Collections_IEnumerator_get_Current_mCBD03C6B95EC8B23F96C796EDDA096B2FDF30F9E,
	DestroyTime_t24806801B4F6EA764796D3C1789E9D157D0023D2_CustomAttributesCacheGenerator_DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9,
	U3CCallDestroyU3Ed__2_tAFEBF82F13F04716C1673744B80612E43A0ACA03_CustomAttributesCacheGenerator_U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0,
	Explosion_t03ACA9CD2F46E4913DC45A3EA50B165F4662DCED_CustomAttributesCacheGenerator_Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7,
	U3CExplodeU3Ed__3_tD50D1FAB07D31C87001D70FBED9046F4B147664D_CustomAttributesCacheGenerator_U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB,
	ControlShip_tDE8B063AEEACBC4110A6A51AF8533BED76E0D3C8_CustomAttributesCacheGenerator_ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA,
	U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator_U3CShootU3Ed__22__ctor_m0BF6C55FBDCC191AF04C596B1E91FF554E136AF0,
	U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator_U3CShootU3Ed__22_System_IDisposable_Dispose_m29F681C63A24CE8EF48A0EE0B680713DCD4CC835,
	U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator_U3CShootU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75B501F2E6338D92511F4038F1A414A1EFB1577A,
	U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator_U3CShootU3Ed__22_System_Collections_IEnumerator_Reset_m8D390AA67F846A37CC2FCBF11146A6AC77B07BCD,
	U3CShootU3Ed__22_t90F163E0D03A972C52C1C3D4220C717477A8C466_CustomAttributesCacheGenerator_U3CShootU3Ed__22_System_Collections_IEnumerator_get_Current_m6C3A51F868B3F9FBD857AF22BA6B73D84431D652,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
