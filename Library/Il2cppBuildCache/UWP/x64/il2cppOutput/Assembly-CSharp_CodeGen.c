﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Life::TakesLife(System.Int32)
extern void Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD (void);
// 0x00000002 System.Void Life::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178 (void);
// 0x00000003 System.Void Life::.ctor()
extern void Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926 (void);
// 0x00000004 System.Void ControlGame::Start()
extern void ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225 (void);
// 0x00000005 System.Void ControlGame::Update()
extern void ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50 (void);
// 0x00000006 System.Void ControlGame::LevelPassed()
extern void ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534 (void);
// 0x00000007 System.Void ControlGame::GameOver()
extern void ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13 (void);
// 0x00000008 System.Void ControlGame::Clear()
extern void ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074 (void);
// 0x00000009 System.Void ControlGame::PauseGame()
extern void ControlGame_PauseGame_m341859C0FD7778EBDA18E51802B2BDBACA7F8F16 (void);
// 0x0000000A System.Void ControlGame::PauseAudioSources()
extern void ControlGame_PauseAudioSources_m48D0469C1F168C8DC79A3BD8EFEFC3A6DB72A481 (void);
// 0x0000000B System.Void ControlGame::PlayAudioSources()
extern void ControlGame_PlayAudioSources_m9576B6C97C961787AF3DCA9D96967B15913ADD97 (void);
// 0x0000000C System.Void ControlGame::.ctor()
extern void ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D (void);
// 0x0000000D System.Void ControlStart::Start()
extern void ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51 (void);
// 0x0000000E System.Void ControlStart::StartClick()
extern void ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB (void);
// 0x0000000F System.Void ControlStart::Continue()
extern void ControlStart_Continue_m3B448B76CFDA49B027673165D67AC25B437BFC8C (void);
// 0x00000010 System.Void ControlStart::Quit()
extern void ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747 (void);
// 0x00000011 System.Void ControlStart::.ctor()
extern void ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA (void);
// 0x00000012 System.Void EnemyControl::Start()
extern void EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F (void);
// 0x00000013 System.Collections.IEnumerator EnemyControl::Process()
extern void EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B (void);
// 0x00000014 System.Void EnemyControl::EnemyCreate()
extern void EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E (void);
// 0x00000015 System.Collections.IEnumerator EnemyControl::CallBoss()
extern void EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101 (void);
// 0x00000016 System.Void EnemyControl::.ctor()
extern void EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF (void);
// 0x00000017 System.Void EnemyControl/<Process>d__5::.ctor(System.Int32)
extern void U3CProcessU3Ed__5__ctor_m9EB907EB96FE898721FBC65CE3C54E7CFAA5C9B5 (void);
// 0x00000018 System.Void EnemyControl/<Process>d__5::System.IDisposable.Dispose()
extern void U3CProcessU3Ed__5_System_IDisposable_Dispose_m00B8B03C83A0EABF699CBF08F14626CFB04183BB (void);
// 0x00000019 System.Boolean EnemyControl/<Process>d__5::MoveNext()
extern void U3CProcessU3Ed__5_MoveNext_m455168326F7C658C46A21A3A581BB557FC480928 (void);
// 0x0000001A System.Object EnemyControl/<Process>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E271193F0B2FD02B2FEE648C61EF68D6D4D0501 (void);
// 0x0000001B System.Void EnemyControl/<Process>d__5::System.Collections.IEnumerator.Reset()
extern void U3CProcessU3Ed__5_System_Collections_IEnumerator_Reset_mD794B85236A6D135A35F1E2D251AEC1350FF1BB7 (void);
// 0x0000001C System.Object EnemyControl/<Process>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CProcessU3Ed__5_System_Collections_IEnumerator_get_Current_m3E0C3125756F18616C112D57F2F5240EAECAB7FD (void);
// 0x0000001D System.Void EnemyControl/<CallBoss>d__7::.ctor(System.Int32)
extern void U3CCallBossU3Ed__7__ctor_m6306AC9036C8D72FD585EE9EF3D3BDE61257C853 (void);
// 0x0000001E System.Void EnemyControl/<CallBoss>d__7::System.IDisposable.Dispose()
extern void U3CCallBossU3Ed__7_System_IDisposable_Dispose_m7DC3BD63F213D4A573FAAC214A03C43D9BBB3422 (void);
// 0x0000001F System.Boolean EnemyControl/<CallBoss>d__7::MoveNext()
extern void U3CCallBossU3Ed__7_MoveNext_m0D610D843888C01E2630B43F32A199ECC1F37818 (void);
// 0x00000020 System.Object EnemyControl/<CallBoss>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallBossU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AEB18A69A9D6490183794EE916A477716BA54AD (void);
// 0x00000021 System.Void EnemyControl/<CallBoss>d__7::System.Collections.IEnumerator.Reset()
extern void U3CCallBossU3Ed__7_System_Collections_IEnumerator_Reset_m7F8D5D398C1810F019D5BFD7FF9FC95AC91A7A4A (void);
// 0x00000022 System.Object EnemyControl/<CallBoss>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CCallBossU3Ed__7_System_Collections_IEnumerator_get_Current_m6EA8178F2E1F71F2D655440F4A8FB0A79F9F5BAA (void);
// 0x00000023 System.Void RecordControl::Start()
extern void RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F (void);
// 0x00000024 System.Void RecordControl::UpdateName()
extern void RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC (void);
// 0x00000025 System.Void RecordControl::.ctor()
extern void RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4 (void);
// 0x00000026 System.Void Boss2::Start()
extern void Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8 (void);
// 0x00000027 System.Collections.IEnumerator Boss2::ShowParts()
extern void Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315 (void);
// 0x00000028 System.Void Boss2::Update()
extern void Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14 (void);
// 0x00000029 System.Collections.IEnumerator Boss2::Shot()
extern void Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6 (void);
// 0x0000002A UnityEngine.GameObject Boss2::GetOneActive()
extern void Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3 (void);
// 0x0000002B System.Void Boss2::KillMe(UnityEngine.GameObject)
extern void Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285 (void);
// 0x0000002C System.Void Boss2::.ctor()
extern void Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F (void);
// 0x0000002D System.Void Boss2/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8 (void);
// 0x0000002E System.Void Boss2/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146 (void);
// 0x0000002F System.Boolean Boss2/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704 (void);
// 0x00000030 System.Object Boss2/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41 (void);
// 0x00000031 System.Void Boss2/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D (void);
// 0x00000032 System.Object Boss2/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A (void);
// 0x00000033 System.Void Boss2/<Shot>d__7::.ctor(System.Int32)
extern void U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C (void);
// 0x00000034 System.Void Boss2/<Shot>d__7::System.IDisposable.Dispose()
extern void U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7 (void);
// 0x00000035 System.Boolean Boss2/<Shot>d__7::MoveNext()
extern void U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE (void);
// 0x00000036 System.Object Boss2/<Shot>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6 (void);
// 0x00000037 System.Void Boss2/<Shot>d__7::System.Collections.IEnumerator.Reset()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62 (void);
// 0x00000038 System.Object Boss2/<Shot>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25 (void);
// 0x00000039 System.Void Boss3::Start()
extern void Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C (void);
// 0x0000003A System.Collections.IEnumerator Boss3::ShowParts(System.Boolean)
extern void Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6 (void);
// 0x0000003B System.Collections.IEnumerator Boss3::AttackNow()
extern void Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420 (void);
// 0x0000003C System.Collections.IEnumerator Boss3::Attack(UnityEngine.UI.Image)
extern void Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006 (void);
// 0x0000003D UnityEngine.GameObject Boss3::GetOneActive()
extern void Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F (void);
// 0x0000003E System.Void Boss3::KillMe(UnityEngine.GameObject)
extern void Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029 (void);
// 0x0000003F System.Void Boss3::.ctor()
extern void Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306 (void);
// 0x00000040 System.Void Boss3/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D (void);
// 0x00000041 System.Void Boss3/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962 (void);
// 0x00000042 System.Boolean Boss3/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77 (void);
// 0x00000043 System.Object Boss3/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F (void);
// 0x00000044 System.Void Boss3/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524 (void);
// 0x00000045 System.Object Boss3/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF (void);
// 0x00000046 System.Void Boss3/<AttackNow>d__6::.ctor(System.Int32)
extern void U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9 (void);
// 0x00000047 System.Void Boss3/<AttackNow>d__6::System.IDisposable.Dispose()
extern void U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521 (void);
// 0x00000048 System.Boolean Boss3/<AttackNow>d__6::MoveNext()
extern void U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7 (void);
// 0x00000049 System.Object Boss3/<AttackNow>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA (void);
// 0x0000004A System.Void Boss3/<AttackNow>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A (void);
// 0x0000004B System.Object Boss3/<AttackNow>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0 (void);
// 0x0000004C System.Void Boss3/<Attack>d__7::.ctor(System.Int32)
extern void U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327 (void);
// 0x0000004D System.Void Boss3/<Attack>d__7::System.IDisposable.Dispose()
extern void U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF (void);
// 0x0000004E System.Boolean Boss3/<Attack>d__7::MoveNext()
extern void U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301 (void);
// 0x0000004F System.Object Boss3/<Attack>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52 (void);
// 0x00000050 System.Void Boss3/<Attack>d__7::System.Collections.IEnumerator.Reset()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7 (void);
// 0x00000051 System.Object Boss3/<Attack>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F (void);
// 0x00000052 System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x00000053 System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x00000054 System.Collections.IEnumerator Enemy::DodgeToNewPosition()
extern void Enemy_DodgeToNewPosition_m6F09C0B0C29172A7AE00EA4BA82B70C409FB5FEB (void);
// 0x00000055 System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12 (void);
// 0x00000056 System.Void Enemy::MyDeath()
extern void Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8 (void);
// 0x00000057 System.Collections.IEnumerator Enemy::KillMe()
extern void Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5 (void);
// 0x00000058 System.Void Enemy::Create(System.Int32)
extern void Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759 (void);
// 0x00000059 System.Collections.IEnumerator Enemy::Shoot()
extern void Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767 (void);
// 0x0000005A System.Void Enemy::EndBoss()
extern void Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA (void);
// 0x0000005B System.Void Enemy::PStick()
extern void Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173 (void);
// 0x0000005C System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x0000005D System.Void Enemy/<DodgeToNewPosition>d__15::.ctor(System.Int32)
extern void U3CDodgeToNewPositionU3Ed__15__ctor_m0E5A97B8E139B274C51E2ED3DA28C562162E085B (void);
// 0x0000005E System.Void Enemy/<DodgeToNewPosition>d__15::System.IDisposable.Dispose()
extern void U3CDodgeToNewPositionU3Ed__15_System_IDisposable_Dispose_m2BD9D44640AD35F0443D6382F8BCFA02C81B8548 (void);
// 0x0000005F System.Boolean Enemy/<DodgeToNewPosition>d__15::MoveNext()
extern void U3CDodgeToNewPositionU3Ed__15_MoveNext_mCE6054CF6FD47106D574AC1621A566B2F246248F (void);
// 0x00000060 System.Object Enemy/<DodgeToNewPosition>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDodgeToNewPositionU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47EFDDA5A8A24A0A3E180713F136D9DF1DBDDFFA (void);
// 0x00000061 System.Void Enemy/<DodgeToNewPosition>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDodgeToNewPositionU3Ed__15_System_Collections_IEnumerator_Reset_m87CA9FF4689F13B383F7A25D6B833BAA192862F0 (void);
// 0x00000062 System.Object Enemy/<DodgeToNewPosition>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDodgeToNewPositionU3Ed__15_System_Collections_IEnumerator_get_Current_mDE156AF9CFC0DA387BE2142E40AE320BC8C5F463 (void);
// 0x00000063 System.Void Enemy/<KillMe>d__18::.ctor(System.Int32)
extern void U3CKillMeU3Ed__18__ctor_mB6D7E5ADBA40D740E62FAEEB83A7F8A95AECBF62 (void);
// 0x00000064 System.Void Enemy/<KillMe>d__18::System.IDisposable.Dispose()
extern void U3CKillMeU3Ed__18_System_IDisposable_Dispose_m7EE76697DA279F061C9E2CB673439B3B9AD61B05 (void);
// 0x00000065 System.Boolean Enemy/<KillMe>d__18::MoveNext()
extern void U3CKillMeU3Ed__18_MoveNext_mDBD9EA851011C4D05187506A28EDD677D88D2332 (void);
// 0x00000066 System.Object Enemy/<KillMe>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKillMeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m608652410A36CEA3719C34427C8FDBCBD80351D1 (void);
// 0x00000067 System.Void Enemy/<KillMe>d__18::System.Collections.IEnumerator.Reset()
extern void U3CKillMeU3Ed__18_System_Collections_IEnumerator_Reset_mBFA75F8939D22AA066732B49BF854724496D7CC7 (void);
// 0x00000068 System.Object Enemy/<KillMe>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CKillMeU3Ed__18_System_Collections_IEnumerator_get_Current_m1B6B8DE350B6F946FA80D6E56B06F197B2C38621 (void);
// 0x00000069 System.Void Enemy/<Shoot>d__20::.ctor(System.Int32)
extern void U3CShootU3Ed__20__ctor_mA4F447BAD55E437F71D077495DA28DC1511039C6 (void);
// 0x0000006A System.Void Enemy/<Shoot>d__20::System.IDisposable.Dispose()
extern void U3CShootU3Ed__20_System_IDisposable_Dispose_mA84149F7BF21951275C5216F3E6F69331CA5F101 (void);
// 0x0000006B System.Boolean Enemy/<Shoot>d__20::MoveNext()
extern void U3CShootU3Ed__20_MoveNext_mC6AAA5541DFEEC8C05089113F69C9294B54A48A1 (void);
// 0x0000006C System.Object Enemy/<Shoot>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6299308E86DC71DFB95538A4E96554BC36FDE9ED (void);
// 0x0000006D System.Void Enemy/<Shoot>d__20::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__20_System_Collections_IEnumerator_Reset_m830371A449507FC6B227D7EEAAD7643E4776D655 (void);
// 0x0000006E System.Object Enemy/<Shoot>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__20_System_Collections_IEnumerator_get_Current_mCBD03C6B95EC8B23F96C796EDDA096B2FDF30F9E (void);
// 0x0000006F System.Void ItemDrop::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7 (void);
// 0x00000070 System.Void ItemDrop::.ctor()
extern void ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2 (void);
// 0x00000071 System.Void CallScene::Call(System.String)
extern void CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE (void);
// 0x00000072 System.Void CallScene::.ctor()
extern void CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C (void);
// 0x00000073 System.Void DestroyTime::Start()
extern void DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497 (void);
// 0x00000074 System.Collections.IEnumerator DestroyTime::CallDestroy()
extern void DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6 (void);
// 0x00000075 System.Void DestroyTime::.ctor()
extern void DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88 (void);
// 0x00000076 System.Void DestroyTime/<CallDestroy>d__2::.ctor(System.Int32)
extern void U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF (void);
// 0x00000077 System.Void DestroyTime/<CallDestroy>d__2::System.IDisposable.Dispose()
extern void U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD (void);
// 0x00000078 System.Boolean DestroyTime/<CallDestroy>d__2::MoveNext()
extern void U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B (void);
// 0x00000079 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2 (void);
// 0x0000007A System.Void DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9 (void);
// 0x0000007B System.Object DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0 (void);
// 0x0000007C System.Void Explosion::Start()
extern void Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22 (void);
// 0x0000007D System.Collections.IEnumerator Explosion::Explode()
extern void Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258 (void);
// 0x0000007E System.Void Explosion::.ctor()
extern void Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094 (void);
// 0x0000007F System.Void Explosion/<Explode>d__3::.ctor(System.Int32)
extern void U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F (void);
// 0x00000080 System.Void Explosion/<Explode>d__3::System.IDisposable.Dispose()
extern void U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8 (void);
// 0x00000081 System.Boolean Explosion/<Explode>d__3::MoveNext()
extern void U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC (void);
// 0x00000082 System.Object Explosion/<Explode>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02 (void);
// 0x00000083 System.Void Explosion/<Explode>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7 (void);
// 0x00000084 System.Object Explosion/<Explode>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB (void);
// 0x00000085 System.Void MBuildProcessor::.ctor()
extern void MBuildProcessor__ctor_m06E227BDC15A7F4549638B59EF49C4AFF6844D76 (void);
// 0x00000086 Stick/stck Stick::GetStck()
extern void Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B (void);
// 0x00000087 System.Void ControlShip::Start()
extern void ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8 (void);
// 0x00000088 System.Void ControlShip::Update()
extern void ControlShip_Update_m8BEA7F38DEBFB80F1CB809F781E52E524EB1F0BE (void);
// 0x00000089 System.Void ControlShip::LateUpdate()
extern void ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D (void);
// 0x0000008A System.Void ControlShip::AnimateMotor()
extern void ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27 (void);
// 0x0000008B System.Void ControlShip::AddBulletToDogeEnemys(UnityEngine.Transform)
extern void ControlShip_AddBulletToDogeEnemys_m396841E1C79F433DE1ADEB0B91072A9A5035650A (void);
// 0x0000008C System.Void ControlShip::RemoveBulletToDodgeEnemys(UnityEngine.Transform)
extern void ControlShip_RemoveBulletToDodgeEnemys_mD352D26A8FE9D873E9F2A73457030E9F09B8C9CF (void);
// 0x0000008D System.Collections.IEnumerator ControlShip::Shoot()
extern void ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA (void);
// 0x0000008E System.Void ControlShip::CallShield()
extern void ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C (void);
// 0x0000008F System.Void ControlShip::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73 (void);
// 0x00000090 System.Void ControlShip::.ctor()
extern void ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93 (void);
// 0x00000091 System.Void ControlShip/<Shoot>d__22::.ctor(System.Int32)
extern void U3CShootU3Ed__22__ctor_m0BF6C55FBDCC191AF04C596B1E91FF554E136AF0 (void);
// 0x00000092 System.Void ControlShip/<Shoot>d__22::System.IDisposable.Dispose()
extern void U3CShootU3Ed__22_System_IDisposable_Dispose_m29F681C63A24CE8EF48A0EE0B680713DCD4CC835 (void);
// 0x00000093 System.Boolean ControlShip/<Shoot>d__22::MoveNext()
extern void U3CShootU3Ed__22_MoveNext_mF8A69681F63F15F65E55AD29C9D8D41F696B7E6A (void);
// 0x00000094 System.Object ControlShip/<Shoot>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75B501F2E6338D92511F4038F1A414A1EFB1577A (void);
// 0x00000095 System.Void ControlShip/<Shoot>d__22::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__22_System_Collections_IEnumerator_Reset_m8D390AA67F846A37CC2FCBF11146A6AC77B07BCD (void);
// 0x00000096 System.Object ControlShip/<Shoot>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__22_System_Collections_IEnumerator_get_Current_m6C3A51F868B3F9FBD857AF22BA6B73D84431D652 (void);
// 0x00000097 System.Void Flux.ReadMe::.ctor()
extern void ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56 (void);
// 0x00000098 System.Void Gaminho.Level::.ctor()
extern void Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9 (void);
// 0x00000099 System.Void Gaminho.ScenarioLimits::.ctor()
extern void ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B (void);
// 0x0000009A System.Void Gaminho.Shot::.ctor()
extern void Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3 (void);
// 0x0000009B UnityEngine.Quaternion Gaminho.Statics::FaceObject(UnityEngine.Vector2,UnityEngine.Vector2,Gaminho.Statics/FacingDirection)
extern void Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50 (void);
// 0x0000009C System.Void Gaminho.Statics::.cctor()
extern void Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4 (void);
static Il2CppMethodPointer s_methodPointers[156] = 
{
	Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD,
	Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178,
	Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926,
	ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225,
	ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50,
	ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534,
	ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13,
	ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074,
	ControlGame_PauseGame_m341859C0FD7778EBDA18E51802B2BDBACA7F8F16,
	ControlGame_PauseAudioSources_m48D0469C1F168C8DC79A3BD8EFEFC3A6DB72A481,
	ControlGame_PlayAudioSources_m9576B6C97C961787AF3DCA9D96967B15913ADD97,
	ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D,
	ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51,
	ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB,
	ControlStart_Continue_m3B448B76CFDA49B027673165D67AC25B437BFC8C,
	ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747,
	ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA,
	EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F,
	EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B,
	EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E,
	EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101,
	EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF,
	U3CProcessU3Ed__5__ctor_m9EB907EB96FE898721FBC65CE3C54E7CFAA5C9B5,
	U3CProcessU3Ed__5_System_IDisposable_Dispose_m00B8B03C83A0EABF699CBF08F14626CFB04183BB,
	U3CProcessU3Ed__5_MoveNext_m455168326F7C658C46A21A3A581BB557FC480928,
	U3CProcessU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3E271193F0B2FD02B2FEE648C61EF68D6D4D0501,
	U3CProcessU3Ed__5_System_Collections_IEnumerator_Reset_mD794B85236A6D135A35F1E2D251AEC1350FF1BB7,
	U3CProcessU3Ed__5_System_Collections_IEnumerator_get_Current_m3E0C3125756F18616C112D57F2F5240EAECAB7FD,
	U3CCallBossU3Ed__7__ctor_m6306AC9036C8D72FD585EE9EF3D3BDE61257C853,
	U3CCallBossU3Ed__7_System_IDisposable_Dispose_m7DC3BD63F213D4A573FAAC214A03C43D9BBB3422,
	U3CCallBossU3Ed__7_MoveNext_m0D610D843888C01E2630B43F32A199ECC1F37818,
	U3CCallBossU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AEB18A69A9D6490183794EE916A477716BA54AD,
	U3CCallBossU3Ed__7_System_Collections_IEnumerator_Reset_m7F8D5D398C1810F019D5BFD7FF9FC95AC91A7A4A,
	U3CCallBossU3Ed__7_System_Collections_IEnumerator_get_Current_m6EA8178F2E1F71F2D655440F4A8FB0A79F9F5BAA,
	RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F,
	RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC,
	RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4,
	Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8,
	Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315,
	Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14,
	Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6,
	Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3,
	Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285,
	Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F,
	U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146,
	U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A,
	U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C,
	U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7,
	U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE,
	U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6,
	U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62,
	U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25,
	Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C,
	Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6,
	Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420,
	Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006,
	Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F,
	Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029,
	Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306,
	U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962,
	U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF,
	U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9,
	U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521,
	U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7,
	U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0,
	U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327,
	U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF,
	U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301,
	U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_DodgeToNewPosition_m6F09C0B0C29172A7AE00EA4BA82B70C409FB5FEB,
	Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12,
	Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8,
	Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5,
	Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759,
	Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767,
	Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA,
	Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	U3CDodgeToNewPositionU3Ed__15__ctor_m0E5A97B8E139B274C51E2ED3DA28C562162E085B,
	U3CDodgeToNewPositionU3Ed__15_System_IDisposable_Dispose_m2BD9D44640AD35F0443D6382F8BCFA02C81B8548,
	U3CDodgeToNewPositionU3Ed__15_MoveNext_mCE6054CF6FD47106D574AC1621A566B2F246248F,
	U3CDodgeToNewPositionU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m47EFDDA5A8A24A0A3E180713F136D9DF1DBDDFFA,
	U3CDodgeToNewPositionU3Ed__15_System_Collections_IEnumerator_Reset_m87CA9FF4689F13B383F7A25D6B833BAA192862F0,
	U3CDodgeToNewPositionU3Ed__15_System_Collections_IEnumerator_get_Current_mDE156AF9CFC0DA387BE2142E40AE320BC8C5F463,
	U3CKillMeU3Ed__18__ctor_mB6D7E5ADBA40D740E62FAEEB83A7F8A95AECBF62,
	U3CKillMeU3Ed__18_System_IDisposable_Dispose_m7EE76697DA279F061C9E2CB673439B3B9AD61B05,
	U3CKillMeU3Ed__18_MoveNext_mDBD9EA851011C4D05187506A28EDD677D88D2332,
	U3CKillMeU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m608652410A36CEA3719C34427C8FDBCBD80351D1,
	U3CKillMeU3Ed__18_System_Collections_IEnumerator_Reset_mBFA75F8939D22AA066732B49BF854724496D7CC7,
	U3CKillMeU3Ed__18_System_Collections_IEnumerator_get_Current_m1B6B8DE350B6F946FA80D6E56B06F197B2C38621,
	U3CShootU3Ed__20__ctor_mA4F447BAD55E437F71D077495DA28DC1511039C6,
	U3CShootU3Ed__20_System_IDisposable_Dispose_mA84149F7BF21951275C5216F3E6F69331CA5F101,
	U3CShootU3Ed__20_MoveNext_mC6AAA5541DFEEC8C05089113F69C9294B54A48A1,
	U3CShootU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6299308E86DC71DFB95538A4E96554BC36FDE9ED,
	U3CShootU3Ed__20_System_Collections_IEnumerator_Reset_m830371A449507FC6B227D7EEAAD7643E4776D655,
	U3CShootU3Ed__20_System_Collections_IEnumerator_get_Current_mCBD03C6B95EC8B23F96C796EDDA096B2FDF30F9E,
	ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7,
	ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2,
	CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE,
	CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C,
	DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497,
	DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6,
	DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88,
	U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF,
	U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD,
	U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B,
	U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0,
	Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22,
	Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258,
	Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094,
	U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F,
	U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8,
	U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC,
	U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB,
	MBuildProcessor__ctor_m06E227BDC15A7F4549638B59EF49C4AFF6844D76,
	Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B,
	ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8,
	ControlShip_Update_m8BEA7F38DEBFB80F1CB809F781E52E524EB1F0BE,
	ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D,
	ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27,
	ControlShip_AddBulletToDogeEnemys_m396841E1C79F433DE1ADEB0B91072A9A5035650A,
	ControlShip_RemoveBulletToDodgeEnemys_mD352D26A8FE9D873E9F2A73457030E9F09B8C9CF,
	ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA,
	ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C,
	ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73,
	ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93,
	U3CShootU3Ed__22__ctor_m0BF6C55FBDCC191AF04C596B1E91FF554E136AF0,
	U3CShootU3Ed__22_System_IDisposable_Dispose_m29F681C63A24CE8EF48A0EE0B680713DCD4CC835,
	U3CShootU3Ed__22_MoveNext_mF8A69681F63F15F65E55AD29C9D8D41F696B7E6A,
	U3CShootU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75B501F2E6338D92511F4038F1A414A1EFB1577A,
	U3CShootU3Ed__22_System_Collections_IEnumerator_Reset_m8D390AA67F846A37CC2FCBF11146A6AC77B07BCD,
	U3CShootU3Ed__22_System_Collections_IEnumerator_get_Current_m6C3A51F868B3F9FBD857AF22BA6B73D84431D652,
	ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56,
	Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9,
	ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B,
	Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3,
	Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50,
	Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4,
};
static const int32_t s_InvokerIndices[156] = 
{
	1229,
	1240,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1413,
	1445,
	1413,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1445,
	1445,
	1445,
	1413,
	1445,
	1413,
	1413,
	1240,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1006,
	1413,
	1005,
	1413,
	1240,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1445,
	1413,
	1240,
	1445,
	1413,
	1229,
	1413,
	1445,
	1445,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1240,
	1445,
	1240,
	1445,
	1445,
	1413,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1413,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	2344,
	1445,
	1445,
	1445,
	1445,
	2324,
	2324,
	1413,
	1445,
	1240,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1445,
	1445,
	1445,
	1875,
	2365,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	156,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
